### Summary

Disable GNOME Shell background update check for extensions, without patching extension manifest or uninstalling GNOME Extensions software.

This very basic extension simply hooks into [`checkForUpdates`](https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/ae90b50dc7237943af20af2a3e8cb00f4e20172e/js/ui/extensionDownloader.js#L159) in `ui/extensionDownloader` and always returns. The idea is from [a comment in relevant issue](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/2514#note_1090636).

### Tutorial

To install it, simply do this (You may want to audit it first):

```bash
git clone https://gitlab.com/thjderjktyrjkt/disable-gnome-extension-update-check.git "$HOME/.local/share/gnome-shell/extensions/disable-gnome-extension-update-check@thjderjktyrjkt.gitlab.com"
```

Then restart your GNOME Shell. On X.org you can press `Alt + F2`, type `r` and press Enter. On Wayland you need to logout and re-login.

After that you can enable this extension in GNOME Extensions software.

To uninstall it:

```bash
rm --recursive "$HOME/.local/share/gnome-shell/extensions/disable-gnome-extension-update-check@thjderjktyrjkt.gitlab.com"
```

### Updates

##### 2

Don't deactivate when lock screen is shown.


---

### Backgrounds

Why disable it:

- It phones home: I don't want extensions.gnome.org to know my installed extensions list. Tranditional package manager doesn't have this issue as the repo data is downloaded and updates are checked locally (You still have to download the packages but the information leakage is still much fewer).
- It breaks my patches: Everytime GNOME updates my extensions, I have to apply my own extension patches again.
- It introduces single-point-of-failure: Since GNOME Shell doesn't containerize or limit what an extension can do (like modern web browsers), if extensions.gnome.org / extension maintainer's account got hacked and malicious updates are pushed to users, it automatically means all GNOME extension users are punked.

