// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const extensionDownloader = imports.ui.extensionDownloader;

class Extension {
  constructor() {
    this.origMethods = {
      "extensionDownloader.checkForUpdates": extensionDownloader.checkForUpdates
    };
    extensionDownloader.checkForUpdates = () => {
      log("GNOME Shell is trying to check for extension updates, blocking it.");
      return;
    };
  }

  destroy() {
    extensionDownloader.checkForUpdates = this.origMethods["extensionDownloader.checkForUpdates"];
  }

}

let extension = null;

/* exported enable */
function enable() {
  extension = new Extension();
}

/* exported disable */
function disable() {
  extension.destroy();
  extension = null;
}

